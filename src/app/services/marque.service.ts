import { Injectable } from '@angular/core';
import {Marque} from '../models/marque';
import {Voiture} from '../models/voiture';

@Injectable({
  providedIn: 'root'
})
export class MarqueService {

  marques = [
    new Marque('Peugeot', [
      new Voiture( '206', new Date(), 500 ),
      new Voiture( '307', new Date(), 1500 )
    ]),
    new Marque('Renault', [
        new Voiture( 'Clio', new Date(), 5000 ),
        new Voiture( 'Megane', new Date(), 15000 )
      ])
  ];
  constructor() { }

  getAll(): Marque[] {
    return this.marques;
  }

  getVoitureByMarque(nomMarque): Voiture[] {
    let marque =  this.marques.filter(marque => marque.nom === nomMarque)[0];
    return marque.voitures;
  }
}
