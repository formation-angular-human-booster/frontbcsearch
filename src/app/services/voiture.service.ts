import { Injectable } from '@angular/core';
import {Voiture} from '../models/voiture';

@Injectable({
  providedIn: 'root'
})
export class VoitureService {
  voitures = [
    new Voiture( '206', new Date(), 500 ),
    new Voiture( '307', new Date(), 1500 ),
    new Voiture( 'Clio', new Date(), 5000 ),
    new Voiture( 'Megane', new Date(), 15000 )
  ];
  constructor() { }
  getAll(): Voiture[] {
    return this.voitures;
  }

}
