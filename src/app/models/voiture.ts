import {Marque} from './marque';

export class Voiture {

  model: string;
  miseEnCirculation: Date;
  kilometrage: number;

  constructor( model: string = null, miseEnCirculation: Date = null, kilometrage: number = null) {

    this.model = model;
    this.miseEnCirculation = miseEnCirculation;
    this.kilometrage = kilometrage;
  }
}
