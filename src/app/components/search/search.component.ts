import { Component, OnInit } from '@angular/core';
import {Voiture} from '../../models/voiture';
import {Marque} from '../../models/marque';
import {MarqueService} from '../../services/marque.service';
import {VoitureService} from '../../services/voiture.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  marques: Marque[];
  vehiculs: Voiture[];
  marque: Marque;
  constructor(private marqueService: MarqueService, private vehiculsService: VoitureService) { }

  ngOnInit(): void {
    this.marques = this.marqueService.getAll();
    this.vehiculs = this.vehiculsService.getAll();

    console.log(this.marques);
  }

  triData() {
    console.log('todo');
  }

  filterModel($event) {
    console.log('value', $event.target.value);
    if ($event.target.value === 'no-data') {
      this.vehiculs = this.vehiculsService.getAll();
    } else {
      this.vehiculs = this.marqueService.getVoitureByMarque($event.target.value);
    }
  }

}
